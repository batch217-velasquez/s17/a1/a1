/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function userInfo (){
		let fullName = prompt ("What is your name?");
		let age = prompt ("How old are you?");
		let location = prompt ("Where do you lived?");

		alert("Thank you for your Input!");

		console.log("Hello, "+ fullName);
		console.log("You are " + age + " years old.");
		console.log("You live in " + location);
		
	}

	userInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function favoriteBandsArtist(){
		let firstArtist = "Silent Sanctuary"
		let secondArtist = "Jason Mraz"
		let thirdArtist = "Jason Derulo"
		let fourthArtist = "The Juans"
		let fifthArtist = "I Belong to the Zoo"

		console.log("1. " + firstArtist);
		console.log("2. " + secondArtist);
		console.log("3. " + thirdArtist);
		console.log("4. " + fourthArtist);
		console.log("5. " + fifthArtist);
		
	}

favoriteBandsArtist();



/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function favoriteMovies(){
		let firstMovie = "Avengers EndGame"
		let secondMovie = "The Curious Case of Benjamin Button"
		let thirdMovie = "Forest Gump"
		let fourthMovie = "Uncharted"
		let fifthMovie = "Kingsman: The Secret Service"

		let ratingBy = "Rotten Tomatoes Rating: "

		let ratingFirst = "94%"
		let ratingSecond = "71%"
		let ratingThird = "71%"
		let ratingFourth = "41%"
		let ratingFifth = "75%"



		console.log("1. " + firstMovie);
		console.log(ratingBy + ratingFirst )
		console.log("2. " + secondMovie);
		console.log(ratingBy + ratingSecond )
		console.log("3. " + thirdMovie);
		console.log(ratingBy + ratingThird )
		console.log("4. " + fourthMovie);
		console.log(ratingBy + ratingFourth )
		console.log("5. " + fifthMovie);
		console.log(ratingBy + ratingFifth )

	}


favoriteMovies();




/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/




let printFriends = function printUsers(){
	
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt ("Enter your first friend's name:"); 
	let friend2 = prompt ("Enter your second friend's name:"); 
	let friend3 = prompt ("Enter your third friend's name:");


	console.log("You are friends with:")
	console.log(friend1)
	console.log(friend2)
	console.log(friend3)


	


};

printFriends();


